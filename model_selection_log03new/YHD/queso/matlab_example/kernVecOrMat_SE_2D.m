function kK = kernVecOrMat_SE_2D( scnLeft, scnRight, sigma_GP, covlen_GP, sigma_meas )

nLeft = size( scnLeft, 1 );
nRight = size( scnRight, 1 );

if min(nLeft, nRight) == 0
    kK = 0;
    return;
end;

kK = zeros( nLeft, nRight );

for i = 1 : nLeft
    for j = 1 : nRight
    
        r2 = (scnLeft(i,1) - scnRight(j,1))^2 + (scnLeft(i,2) - scnRight(j,2))^2;
        kK(i,j) = sigma_GP^2 * exp( -1/2*r2/covlen_GP^2 );
        if( r2 == 0 )
            kK(i,j) = kK(i,j) + sigma_meas;
        end;
        
    end;
end;