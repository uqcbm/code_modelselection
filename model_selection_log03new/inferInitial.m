x0_n = [0.5, 1, 6];
[x_n,fval_n] = fminunc(@MSEtipfun,x0_n); % x_n = [0.4777    0.1223    6.9372] fval_n =  0.0017
t = 0:0.01:30;
y = log(x_n(1)) - x_n(2)*(t - x_n(3)).^2;

x_span_use =  [   2.5126    5.0251    7.5377   10.0503   12.5628   15.0754   17.5879   20.1005   ];
n_state_use = [    -3.1799   -1.1554   -0.7345   -1.8967   -4.6273   -8.8893  -14.6480  -21.8701 ];

 y_n = 0.4777*exp(-0.1223*(t - 6.9372).^2);
% 
% for i = 1:10
%     if y_n(i) < 1e-10
%         y_n(i) = 1e-10;
%     end;
%     if n_state_use(i) < 1e-10
%        n_state_use(i) = 1e-10;
%     end;
% end;
% 
%  MSEval_n = 0;
%  
% for i = 1:10
%     MSEval_n = MSEval_n + (log(y_n(i)) - log(n_state_use(i)))^2;
% end
% 
% MSEval_n = 1/10*MSEval_n; %33.0494
figure;
plot(t,y_n);
hold on;
plot(x_span_use, exp(n_state_use),'s', 'MarkerEdgeColor', 'k',...
                'MarkerFaceColor',[0.5 0.5 0.5],...
                'MarkerSize',10);


%----------------------------------
path = 'truth_figures\';
fsize = 20;

figure;
subplot(2,2,2)
plot(t, y, 'Linewidth', 3);
hold on;
plot(x_span_use, n_state_use,'s', 'MarkerEdgeColor', 'k',...
                'MarkerFaceColor',[0.5 0.5 0.5],...
                'MarkerSize',10);
xlim([0 25]);
%ylim([0 0.6]);
%set(gca,'ytick',0:0.2:0.6,'xtick',0:5:25);
legend('infered initial condition','observation points');
xlabel('distance from the center');
ylabel('tip density');
title('(b) infered initial condition for tip density');
% saveas(gcf,[path,'inferedinitialtip.jpg']);
% saveas(gcf,[path,'inferedinitialtip.eps']);



%-------------------------------------------

x0_h = [0 0];
[x_h,fval_h] = fminunc(@MSEhyphalfun,x0_h); % x_h = [0.0351   1.0516] fval_h =  0.0489
t = 0:0.01:30;
y = -x_h(1)*(t - x_h(2)).^2;

x_span_use =  [0    2.5126    5.0251    7.5377   10.0503   12.5628   15.0754   17.5879   20.1005   22.6131];
h_state_use = [  0   -0.3194   -0.6530   -1.1485   -2.4830   -4.7233   -7.2071   -9.8154  -12.7762  -16.1299];

% 
% y_h = exp(-0.0208*(t - 0.0064).^2);
% 
% for i = 1:10
%     if y_h(i) < 1e-10
%         y_h(i) = 1e-10;
%     end;
%     if h_state_use(i) < 1e-10
%        h_state_use(i) = 1e-10;
%     end;
% end;
% 
%  MSEval_h = 0;
%  
% for i = 1:10
%     MSEval_h = MSEval_h + (log(y_h(i)) - log(h_state_use(i)))^2;
% end
% 
% MSEval_h = 1/10*MSEval_h; %122.2656

subplot(2,2,1);
plot(t,y, 'LineWidth', 3  );
hold on;
plot(x_span_use, h_state_use, 's', 'MarkerEdgeColor' ,'k',...
                'MarkerFaceColor', [0.5 0.5 0.5],...
                'MarkerSize',10);
xlim([0 25]);
%ylim([0 1.5]);
%set(gca,'ytick', 0:0.3:1.5,'xtick',0:5:25); %,fsize,'ytick'
legend('infered initial condition','observation points');
xlabel('distance from the center');
ylabel('hyphal density') %,'fontsize',fsize
title('(a) infered initial condition for hyphal density');
saveas(gcf,[path,'inferedinitialhyphae.jpg']);
saveas(gcf,[path,'inferedinitialhyphae.eps']);



