clear all 
close all
clc

main_model;

path =[pwd,'\data_figures\'];
rawChain_ml


day = 500;
P_pf_update = ip_ml_47_rawChain_unified';

figure;
subplot(2,3,1);
plot(P_pf_update(1,:));
title('v');
subplot(2,3,2);
plot(P_pf_update(2,:));
title('\alpha_{1}');
subplot(2,3,3);
plot(P_pf_update(3,:));
title('\beta_{2}');
subplot(2,3,4);
plot(P_pf_update(4,:));
title('sigma2 for hyphal density');
subplot(2,3,5);
plot(P_pf_update(5,:));
title('sigma2 for tip density');

saveas(gcf,[path,'sampleTrace.jpg']);
saveas(gcf,[path,'sampleTrace.eps']);

figure;
subplot(2,3,1);
hist(P_pf_update(1,:));
title('v');
subplot(2,3,2);
hist(P_pf_update(2,:));
title('\alpha_{1}');
subplot(2,3,3);
hist(P_pf_update(3,:));
title('\beta_{2}');
subplot(2,3,4);
hist(P_pf_update(4,:));
title('sigma2 for hyphal density');
subplot(2,3,5);
hist(P_pf_update(5,:));
title('sigma2 for tip density');

saveas(gcf,[path,'sampleHist.jpg']);
saveas(gcf,[path,'sampleHist.eps']);

for i = 1: model.smps
    [fx, fp, fn] = hyphal_growth_YH(P_pf_update(:,i),day); 
    fn_raw = fn(:,end) + normrnd(0,sqrt(P_pf_update(5,i)),numel(fx),1);
    fn_raw(find(fn_raw <0)) = 0;
    fn_day(i,:) = fn_raw';
    
    fp_raw = fp(:,end) + normrnd(0,sqrt(P_pf_update(4,i)),numel(fx),1);
    fp_raw(find(fp_raw <0)) = 0;
    fp_day(i,:) = fp_raw';
end;

save rawChain_forwarddata.mat

%load rawChain_forwarddata.mat

% for i = 1:numel(fx)
%     [CDF, Points] = ecdf(fp_day(:,i));
%     lowerbound_p(i) = Points(max(find(CDF<0.025)) + 1);
%     upperbound_p(i) = Points(min(find(CDF>0.975)) - 1);
% end;
% 
% for i = 1:numel(fx)
%     [CDF, Points] = ecdf(fn_day(:,i));
%     lowerbound_n(i) = Points(max(find(CDF<0.025)) + 1);
%     upperbound_n(i) = Points(min(find(CDF>0.975)) - 1);
% end;

for i = 1:numel(fx)
    [CDF, Points] = ecdf(fp_day(:,i));
    lowerbound_p(i) = Points(max(find(CDF<0.00000001)) + 1);
    upperbound_p(i) = Points(min(find(CDF>0.99999999)) - 1);
end;

for i = 1:numel(fx)
    [CDF, Points] = ecdf(fn_day(:,i));
    lowerbound_n(i) = Points(max(find(CDF<0.00000001)) + 1);
    upperbound_n(i) = Points(min(find(CDF>0.99999999)) - 1);
end;

[x_span, p_state, n_state] = hyphal_growth_YHD(p_true, day);
figure;
X = [fx, fliplr(fx)];
Y = [upperbound_p, fliplr(lowerbound_p)];
fill(X,Y,'c');
hold on;
plot(x_span,p_state(:,end),'r');
title('hyphal density prediction');
saveas(gcf,[path,'hyphalDensityP_range.jpg']);
saveas(gcf,[path,'hyphalDensityP_range.eps']);

figure;
X = [fx, fliplr(fx)];
Y = [upperbound_n, fliplr(lowerbound_n)];
fill(X,Y,'c');
hold on;
plot(x_span,n_state(:,end),'r');
title('tip density prediction');
saveas(gcf,[path,'tipDensityP_range.jpg']);
saveas(gcf,[path,'tipDensityP_range.eps']);

