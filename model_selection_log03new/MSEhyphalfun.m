function MSEval = MSEhyphalfun( p )
%MSEFUN Summary of this function goes here
%   Detailed explanation goes here
d = p(1);
e = p(2);

MSEval = 0;
x_span_use =  [0    2.5126    5.0251    7.5377   10.0503   12.5628   15.0754   17.5879   20.1005   22.6131];
h_state_use = [0   -0.3194   -0.6530   -1.1485   -2.4830   -4.7233   -7.2071   -9.8154  -12.7762  -16.1299];
for i = 1:10
    MSEval = MSEval + (-d*(x_span_use(i) - e)^2 - h_state_use(i))^2;
end

MSEval = 1/10*MSEval;

