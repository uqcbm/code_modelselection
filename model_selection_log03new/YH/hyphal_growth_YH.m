function [X,P,N] = hyphal_growth_YH(params, day)

% create the mesh
Nx = 200;
Nt = 451;

x = linspace(0,100,Nx);
t = linspace(0.5,5,Nt);

dx = x(2)-x(1);
dt = t(2)-t(1);

% matrices to store the solution
p = zeros(Nx,Nt);
n = zeros(Nx,Nt);

v = params(1);
a1 = params(2);
b2 = params(3);

% initial condition
for i =1:50
    p(i,1) = exp(-0.0351*(x(i) - 1.0516)^2);%exp(-0.01*i^2);
    n(i,1) = 0.4777*exp(-0.1223*(x(i) - 6.9372)^2);%exp(-0.01*i^2);
end;

int_time = day;

% boundary condition
for j = 1:(int_time)
   p(1, j) = 1;    % left boundary
   n(1, j) = 0;    % left boundary
end;

for j = 1:(int_time-1)
   for i = 2:(Nx-1)      % how to fix it??
       p(i, j+1) = dt*v*n(i, j) + p(i, j);
	 %  n(i, j+1) = -dt/dx*n(i+1, j) + (1+ dt + dt/dx)*n(i, j) - dt*n(i, j)*p(i, j); % forward difference
	   n(i, j+1) = -v*dt/(2*dx)*n(i+1, j) + v*dt/(2*dx)*n(i-1, j) + (1+ a1*dt)*n(i, j) - b2*dt*n(i, j)*p(i, j); %central difference
%        if p(i, j+1)<0
%            p(i, j+1)=0;
%        end;
%        if n(i, j+1)<0
%            n(i,j+1)=0;
%        end;
   end;
end;

% generate the profile needed: 1 to calibrate, 2 to validate

P = p(:,1:int_time);
N = n(:,1:int_time);
X = x;







