setenv('GNUTERM', 'x11');
clear all;
close all;
clc;
%% ------------------------------------------------------------------------
% True system
%--------------------------------------------------------------------------
model.m = 2;				    % dim meas model:
model.smps = 5000;
n_obs = 10;


% true parameters
v = 0.8;
y1 = 0.3;
a1 = 0.1;
b2 = 0.3;

% true p
p_true = [ v y1 a1 b2];
%% ------------------------------------------------------------------------
% Generate measurements
%--------------------------------------------------------------------------

day = 501;

[x_span, p_state, n_state] = hyphal_growth_YHD(p_true, day);

x_use = 1:5:50;

n_state_use_raw_1 = n_state(:,51)';
n_state_use_1 = n_state_use_raw_1(x_use);

p_state_use_raw_1 = p_state(:,51)';
p_state_use_1 = p_state_use_raw_1(x_use);

x_span_use = x_span(x_use);

state_use_1 = [p_state_use_1; n_state_use_1];

meas_1 = state_use_1; %+ chol(model.R)'*randn(model.m, n_obs);

for j = 1:n_obs
   for i = 1:model.m
      if meas_1(i,j)<1e-10
	     meas_1(i,j)=1e-10;
	  end;
   end;
end;

meas_1 = log(meas_1);

n_state_use_raw_2 = n_state(:,501)';
n_state_use_2 = n_state_use_raw_2(x_use);

p_state_use_raw_2 = p_state(:,501)';
p_state_use_2 = p_state_use_raw_2(x_use);

x_span_use = x_span(x_use);

state_use_2 = [p_state_use_2; n_state_use_2];

meas_2 = state_use_2; %+ chol(model.R)'*randn(model.m, n_obs);

for j = 1:n_obs
   for i = 1:model.m
      if meas_2(i,j)<1e-10
	     meas_2(i,j)=1e-10;
	  end;
   end;
end;

meas_2 = log(meas_2);



%% ------------------------------------------------------------------------
% Plot truth
%--------------------------------------------------------------------------
path = 'truth_figures\';
fsize = 20;

figure;
subplot(2,2,1);
plot(x_span, p_state(:,51),'--','color', [0.5 0.5 0.5] , 'Linewidth', 3 );
hold on;
plot(x_span_use, exp(meas_1(1,:)), 's','MarkerEdgeColor','k',...
                'MarkerFaceColor','b',...
                'MarkerSize',10);
 xlim([0 25]);
% ylim([0 1.5]);
%set(gca,'ytick',0:0.3:1.5,'xtick',0:5:25);
legend(' hyphal density','observation points');
xlabel('distance from the center');
ylabel('hyphal density')
% saveas(gcf,[path,'InitialhyphaeForCadidate.jpg']);
% saveas(gcf,[path,'InitialhyphaeForCadidate.eps']);
title('(a) hyphal density at t = 0.5');
subplot(2,2,2);
plot(x_span, n_state(:,51), '--','color', [0.5 0.5 0.5],  'Linewidth', 3 );
hold on;
plot(x_span_use, exp(meas_1(2,:)),'s','MarkerEdgeColor','k',...
                'MarkerFaceColor','b',...
                'MarkerSize',10);
xlim([0 25]);
%ylim([0 0.6]);
%set(gca,'ytick',0:0.2:0.6,'xtick',0:5:25); %,'FontSize',fsize
legend('tip density','observation points');
xlabel('distance from the center'); %,'fontsize',fsize
ylabel('tip density'); %,'fontsize',fsize
title('(b) tip density at t = 0.5');
% saveas(gcf,[path,'InitialTipForCadidate.jpg']);
% saveas(gcf,[path,'InitialTipForCadidate.eps']);
subplot(2,2,3);
plot(x_span, p_state(:,501),'--','color', [0.5 0.5 0.5] , 'Linewidth', 3);
hold on;
plot(x_span_use, exp(meas_2(1,:)), 's','MarkerEdgeColor','k',...
                'MarkerFaceColor','b',...
                'MarkerSize',10);
xlim([0 25]);
%ylim([0 1.5]);
%set(gca,'ytick',0:0.3:1.5,'xtick',0:5:25);
legend('hyphal density','observation points');
xlabel('distance from the center');
ylabel('hyphal density')
title('(c) hyphal density at t = 5');
% saveas(gcf,[path,'ObshyphaeForCadidate.jpg']);
% saveas(gcf,[path,'ObshyphaeForCadidate.eps']);
subplot(2,2,4);
plot(x_span, n_state(:,501), '--','color', [0.5 0.5 0.5],  'Linewidth', 3 );
hold on;
plot(x_span_use, exp(meas_2(2,:)),'s','MarkerEdgeColor','k',...
                'MarkerFaceColor','b',...
                'MarkerSize',10);
xlim([0 25]);
%ylim([0 0.6]);
%set(gca,'ytick',0:0.2:0.6,'xtick',0:5:25);
legend('tip density','observation points');
xlabel('distance from the center');
ylabel('tip density');
title('(d) tip density at t = 5');
% saveas(gcf,[path,'ObsTipForCadidate.jpg']);
% saveas(gcf,[path,'ObsTipForCadidate.eps']);
saveas(gcf,[path,'observationpoints.jpg']);
saveas(gcf,[path,'observationpoints.eps']);

%% ------------------------------------------------------------------------
% Save measurements
%--------------------------------------------------------------------------


save truth_measurements.mat;

