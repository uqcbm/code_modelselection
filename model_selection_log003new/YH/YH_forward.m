clear all 
close all
clc

YH_main_model;

path =[pwd,'\data_figures\'];
rawChain_ml


day = 451;
P_pf_update = ip_ml_35_rawChain_unified';

figure;
subplot(2,3,1);
plot(P_pf_update(1,:));
title('(a) \upsilon');
subplot(2,3,2);
plot(P_pf_update(2,:));
title('(b) \alpha_{1}');
subplot(2,3,3);
plot(P_pf_update(3,:));
title('(c) \beta_{2}');
subplot(2,3,4);
plot(P_pf_update(4,:));
title('(d) sigma2 for hyphal density');
subplot(2,3,5);
plot(P_pf_update(5,:));
title('(e) sigma2 for tip density');

saveas(gcf,[path,'sampleTrace.jpg']);
saveas(gcf,[path,'sampleTrace.eps']);

figure;
num_bins = 30;
subplot(2,3,1);
hist(P_pf_update(1,:),num_bins);
xlabel('\upsilon');
ylabel('frequency');
subplot(2,3,2);
hist(P_pf_update(2,:),num_bins);
xlabel('\alpha_{1}');
ylabel('frequency');
subplot(2,3,3);
hist(P_pf_update(3,:),num_bins);
xlabel('\beta_{2}');
ylabel('frequency');
subplot(2,3,4);
hist(P_pf_update(4,:),num_bins);
xlabel('\sigma_{\rho}^{2}');
ylabel('frequency');
subplot(2,3,5);
hist(P_pf_update(5,:),num_bins);
xlabel('\sigma_{n}^{2}');
ylabel('frequency');


saveas(gcf,[path,'sampleHist.jpg']);
saveas(gcf,[path,'sampleHist.eps']);

for i = 1: model.smps
    [fx, fp, fn] = hyphal_growth_YH(P_pf_update(:,i),day); 
    fn_raw = fn(:,end).*lognrnd(0,sqrt(P_pf_update(5,i)),numel(fx),1);
  %  fn_raw(find(fn_raw <0)) = 0;
    fn_day(i,:) = fn_raw';
    
    fp_raw = fp(:,end).*lognrnd(0,sqrt(P_pf_update(4,i)),numel(fx),1);
  %  fp_raw(find(fp_raw <0)) = 0;
    fp_day(i,:) = fp_raw';
end;

save rawChain_forwarddata.mat

load rawChain_forwarddata.mat

mean_p = mean(fp_day);
mean_n = mean(fn_day);

for i = 1:numel(fx)
    [CDF, Points] = ecdf(fp_day(:,i));
    lowerbound_p1(i) = Points(max(find(CDF<0.21)) + 1);
    upperbound_p1(i) = Points(min(find(CDF>0.89)) - 1);
end;

for i = 1:numel(fx)
    [CDF, Points] = ecdf(fn_day(:,i));
    lowerbound_n1(i) = Points(max(find(CDF<0.21)) + 1);
    upperbound_n1(i) = Points(min(find(CDF>0.89)) - 1);
end;

for i = 1:numel(fx)
    [CDF, Points] = ecdf(fp_day(:,i));
    lowerbound_p2(i) = Points(max(find(CDF<0.025)) + 1);
    upperbound_p2(i) = Points(min(find(CDF>0.975)) - 1);
end;

for i = 1:numel(fx)
    [CDF, Points] = ecdf(fn_day(:,i));
    lowerbound_n2(i) = Points(max(find(CDF<0.025)) + 1);
    upperbound_n2(i) = Points(min(find(CDF>0.975)) - 1);
end;

%[x_span, p_state, n_state] = hyphal_growth_YHD(p_true, day);

%load truth_measurements.mat

figure;
subplot(2,2,1)
X2 = [fx, fliplr(fx)];
Y2 = [upperbound_p2, fliplr(lowerbound_p2)];
fill(X2,Y2, [0.85,0.85,0.85]);
hold on;
X1 = [fx, fliplr(fx)];
Y1 = [upperbound_p1, fliplr(lowerbound_p1)];
fill(X1,Y1,[0.6,0.6,0.6]);
plot(fx,mean_p,'k', 'LineWidth',2);

x_span_use = [0    2.5126    5.0251    7.5377   10.0503   12.5628   15.0754   17.5879   20.1005   22.6131];
meas_2 = [1.0000    0.7483    0.8296    1.2969    1.0912    0.4179    0.0452    0.0014    0.0000    0.0000;
    0.0000    0.0000    0.0036    0.0690    0.2230    0.2849    0.0600    0.0026    0.0000    0.0000];


plot(x_span_use, meas_2(1,:),'s','MarkerEdgeColor','k',...
                'MarkerFaceColor','b',...
                'MarkerSize',10);

xlim([0 25]);
ylim([0 1.8]);
xlabel('distance from the center');
ylabel('hyphal density');
title('YH: hyphal density prediction');
legend('95% CI','68% CI','mean','data');
% saveas(gcf,[path,'hyphalDensityP_range.jpg']);
% saveas(gcf,[path,'hyphalDensityP_range.eps']);

subplot(2,2,2)
X2 = [fx, fliplr(fx)];
Y2 = [upperbound_n2, fliplr(lowerbound_n2)];
fill(X2,Y2, [0.85,0.85,0.85]);
hold on;
X1 = [fx, fliplr(fx)];
Y1 = [upperbound_n1, fliplr(lowerbound_n1)];
fill(X1,Y1, [0.6,0.6,0.6]);
plot(fx,mean_n,'k','LineWidth',2);


plot(x_span_use, meas_2(2,:),'s','MarkerEdgeColor','k',...
                'MarkerFaceColor','b',...
                'MarkerSize',10);
            
xlim([0 25]);
ylim([0 0.4]);
xlabel('distance from the center');
ylabel('tip density');
title('YH: tip density prediction');
legend('95% CI','68% CI','mean','data');
saveas(gcf,[path,'prediction.jpg']);
saveas(gcf,[path,'prediction.eps']);
savefig('YHprediction.fig');