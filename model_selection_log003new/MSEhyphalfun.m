function MSEval = MSEhyphalfun( p )
%MSEFUN Summary of this function goes here
%   Detailed explanation goes here
d = p(1);
e = p(2);

MSEval = 0;
x_span_use =  [0    2.5126    5.0251    7.5377   10.0503   12.5628   15.0754   17.5879   20.1005   22.6131];
h_state_use = [ 0   -0.2299   -0.1639    0.1173   -0.4561   -2.2812   -5.3930   -9.2037  -12.6668  -16.0680];
for i = 1:10
    MSEval = MSEval + (-d*(x_span_use(i) - e)^2 - h_state_use(i))^2;
end

MSEval = 1/10*MSEval;

