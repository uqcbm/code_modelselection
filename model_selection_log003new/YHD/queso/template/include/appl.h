/*--------------------------------------------------------------------------
 *--------------------------------------------------------------------------
 *
 * Copyright (C) 2008 The PECOS Development Team
 *
 * Please see http://pecos.ices.utexas.edu for more information.
 *
 * This file is part of the QUESO Library (Quantification of Uncertainty
 * for Estimation, Simulation and Optimization).
 *
 * QUESO is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QUESO is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QUESO. If not, see <http://www.gnu.org/licenses/>.
 *
 *--------------------------------------------------------------------------
 *
 * $Id: exStatisticalInverseProblem_appl.h 14596 2010-10-03 15:10:42Z karl $
 *
 * Brief description of this file: 
 * 
 *--------------------------------------------------------------------------
 *-------------------------------------------------------------------------- */

#ifndef __APPL_H__
#define __APPL_H__

#include <likelihood.h>
#include <uqStatisticalInverseProblem.h>
#include <uqCovCond.h>

//#define PRIOR_IS_GAUSSIAN

//********************************************************
// The driving routine: called by main()
//********************************************************
template<class P_V,class P_M>
void 
uqAppl(const uqBaseEnvironmentClass& env)
{
  if (env.fullRank() == 0) {
    std::cout << "Beginning run of 'exStatisticalInverseProblem_example'\n"
              << std::endl;
  }

  //******************************************************
  // Step 1 of 5: Instantiate the parameter space
  // It has dimension equal to 4
  //******************************************************
  if (env.fullRank() == 0) {
    std::cout << "Executing step 1 of 5: instantiation of parameter space ...\n"
              << std::endl;
  }

  uqVectorSpaceClass<P_V,P_M> paramSpaceU(env,"param_",4,NULL);
  uqVectorSpaceClass<P_V,P_M> paramSpaceI(env,"param_",2,NULL);
  uqVectorSpaceClass<P_V,P_M> paramSpaceT(env,"param_",6,NULL);

  //******************************************************
  // Step 2 of 5: Instantiate the parameter domain
  //******************************************************
  if (env.fullRank() == 0) {
    std::cout << "Executing step 2 of 5: instantiation of parameter domain ...\n"
              << std::endl;
  }

  P_V paramMinsU(paramSpaceU.zeroVector());
  paramMinsU[0] = 0.;
  paramMinsU[1] = 0.;
  paramMinsU[2] = 0.;
  paramMinsU[3] = 0.;

  P_V paramMaxsU(paramSpaceU.zeroVector());
  paramMaxsU[0] = 10.;
  paramMaxsU[1] = 10.;
  paramMaxsU[2] = 10.;
  paramMaxsU[3] = 10.;

  uqBoxSubsetClass<P_V,P_M> paramDomainU("param_",paramSpaceU,paramMinsU,paramMaxsU);


  P_V paramMinsI(paramSpaceI.zeroVector());
  paramMinsI[0] = 1e-10;
  paramMinsI[1] = 1e-10;
  
  P_V paramMaxsI(paramSpaceI.zeroVector());
  paramMaxsI[0] = 10.;
  paramMaxsI[1] = 10.;

  uqBoxSubsetClass<P_V,P_M> paramDomainI("param_",paramSpaceI,paramMinsI,paramMaxsI);

  P_V paramMinsT(paramSpaceT.zeroVector());
  paramMinsT[0] = 0.;
  paramMinsT[1] = 0.;
  paramMinsT[2] = 0.;
  paramMinsT[3] = 0.;
  paramMinsT[4] = 1e-10;
  paramMinsT[5] = 1e-10;

  P_V paramMaxsT(paramSpaceT.zeroVector());
  paramMaxsT[0] = 10.;
  paramMaxsT[1] = 10.;
  paramMaxsT[2] = 10.;
  paramMaxsT[3] = 10.;
  paramMaxsT[4] = 10.;
  paramMaxsT[5] = 10.;  
  
  uqBoxSubsetClass<P_V,P_M> paramDomainT("param_",paramSpaceT,paramMinsT,paramMaxsT);
  //******************************************************
  // Step 3 of 5: Instantiate the likelihood function object (data + routine), to be used by QUESO.
  //******************************************************
  if (env.fullRank() == 0) {
    std::cout << "Executing step 3 of 5: instantiation of likelihood function object ...\n"
              << std::endl;
  }

  std::vector<double> StateData;
  std::vector<double> ObsData;
  std::vector<double> Var;

  std::cout<<"1.so far OK\n";

  uqVectorSpaceClass<P_V,P_M> likelihoodSpace(env,"likeli_",20,NULL);
  P_M likelicovMatrix( likelihoodSpace.zeroVector() );
  P_V likeliVector( likelihoodSpace.zeroVector() );
  P_V IntermVector( likelihoodSpace.zeroVector() ); 
  
  std::cout<<"2.so far OK\n";

  double const state[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
  double const obs[] = { 0,-0.2900,-0.1868,0.2600,0.0873,-0.8726,-3.0962,-6.5858,-11.0420,-15.7282,-23.0259,-23.0259,-5.6221,-2.6733,-1.5006,-1.2558,-2.8141,-5.9656,-10.3091,-15.6846};
  double const stdVar[] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};

  std :: size_t const n = sizeof(state)/sizeof(*state);
  StateData.assign(state, state + n);
  ObsData.assign(obs, obs + n);
  Var.assign(stdVar, stdVar + n);
  

  likelihoodRoutine_DataType<P_V,P_M> likelihoodRoutine_Data;
  likelihoodRoutine_Data.m_StateData           = StateData;
  likelihoodRoutine_Data.m_ObsData             = ObsData;
  likelihoodRoutine_Data.m_Var                 = Var;
  likelihoodRoutine_Data.m_likelicovMatrix     = &likelicovMatrix;
  likelihoodRoutine_Data.m_likeliVector        = &likeliVector;
  likelihoodRoutine_Data.m_IntermVector        = &IntermVector;

  std::cout<<"3.so far OK\n";

  uqGenericScalarFunctionClass<P_V,P_M> likelihoodFunctionObj("like_",
                                                              paramDomainT,
                                                              likelihoodRoutine<P_V,P_M>,
                                                             (void *) &likelihoodRoutine_Data,
                                                              true); // the routine computes [ln(function)]

  //******************************************************
  // Step 4 of 5: Instantiate the inverse problem
  //******************************************************
  if (env.fullRank() == 0) {
    std::cout << "Executing step 4 of 5: instantiation of inverse problem ...\n"
              << std::endl;
  }

  P_V alphaVector( paramSpaceI.zeroVector() ); 
  alphaVector[0] = 10.;
  alphaVector[1] = 10.;


  P_V betaVector( paramSpaceI.zeroVector() );  
  betaVector[0] = 0.0446;
  betaVector[1] =  0.0018;

  

//  uqGaussianVectorRVClass<P_V,P_M> priorRv("prior_",paramDomain,meanVector,paramcovMatrix);
  

  uqUniformVectorRVClass<P_V,P_M> priorRvU("prior_",paramDomainU);

  uqInverseGammaVectorRVClass<P_V,P_M> priorRvI("prior_",paramDomainI,alphaVector,betaVector);

  uqConcatenatedVectorRVClass<P_V,P_M> priorRv("prior_",priorRvU,priorRvI,paramDomainT);
 
  uqGenericVectorRVClass<P_V,P_M> postRv("post_", // Extra prefix before the default "rv_" prefix
                                         paramSpaceT);

  uqStatisticalInverseProblemClass<P_V,P_M> ip("", // No extra prefix before the default "ip_" prefix
                                               NULL,
                                               priorRv,
                                               likelihoodFunctionObj,
                                               postRv);

  //******************************************************
  // Step 5 of 5: Solve the inverse problem
  //******************************************************
  if (env.fullRank() == 0) {
    std::cout << "Executing step 5 of 5: solution of inverse problem ...\n"
              << std::endl;
  }

  //******************************************************
  // According to options in the input file 'sip.inp', the following output files
  // will be created during the solution of the inverse problem:
  // --> ...
  //******************************************************


  /*P_V paramInitials(paramSpace.zeroVector());
  P_M* proposalCovMatrix = postRv.imageSet().vectorSpace().newProposalMatrix(NULL,&paramInitials);
  ip.solveWithBayesMetropolisHastings(NULL,
                                      paramInitials,
                                      proposalCovMatrix);
  delete proposalCovMatrix;
*/

  ip.solveWithBayesMLSampling();
  std::cout << "Log evidence : " << ip.logEvidence() << std::endl;



/*

  //******************************************************
  // Write data to disk, to be used by 'sip_plot.m' afterwards
  //******************************************************
  if (env.fullRank() == 0) {
    std::cout << "Inverse problem solved. Writing data to disk now ...\n"
              << std::endl;
  }

  char varPrefixName[64+1];
  std::set<unsigned int> auxSet;
  auxSet.insert(0);

  sprintf(varPrefixName,"sip_appl_obsData");
  obsData.subWriteContents(varPrefixName,
                              "outputData/appl_output",
                              UQ_FILE_EXTENSION_FOR_MATLAB_FORMAT,
                              auxSet);
  sprintf(varPrefixName,"sip_appl_covMatrix");
  covMatrix.subWriteContents(varPrefixName,
                              "outputData/appl_output",
                              UQ_FILE_EXTENSION_FOR_MATLAB_FORMAT,
                              auxSet);
  sprintf(varPrefixName,"sip_appl_covMatrixInverse");
  covMatrixInverse.subWriteContents(varPrefixName,
                                     "outputData/appl_output",
                                     UQ_FILE_EXTENSION_FOR_MATLAB_FORMAT,
                                     auxSet);
  //std::cout << "covMatrix = [" << *covMatrix
  //          << "];"
  //          << "\n"
  //          << "covMatrixInverse = [" << *covMatrixInverse
  //          << "];"
  //          << std::endl;

  //******************************************************
  // Write weighted squared norm to disk, to be used by 'sip_plot.m' afterwards
  //******************************************************
  // Define auxVec
  const uqBaseVectorRealizerClass<P_V,P_M>& postRealizer = postRv.realizer();
  uqVectorSpaceClass<P_V,P_M> auxSpace(env,"",postRealizer.subPeriod(),NULL);
  P_V auxVec(auxSpace.zeroVector());

  // Populate auxVec
  P_V tmpVec (paramSpace.zeroVector());
  P_V diffVec(obs_Space.zeroVector());
  for (unsigned int i = 0; i < auxSpace.dimLocal(); ++i) {
    postRealizer.realization(tmpVec);
    //diffVec = tmpVec - obsData;
    auxVec[i] = (tmpVec[0] + tmpVec[1] - obsData[0])*covMatrixInverse(0,0)*(tmpVec[0] + tmpVec[1] - obsData[0]);
  }

  // Write auxVec to disk
  sprintf(varPrefixName,"sip_appl_d");
  auxVec.subWriteContents(varPrefixName,
                          "outputData/appl_output",
                          UQ_FILE_EXTENSION_FOR_MATLAB_FORMAT,
                          auxSet);

  //******************************************************
  // Release memory before leaving routine.
  //******************************************************


  if (env.fullRank() == 0) {
    std::cout << "Finishing run of 'exStatisticalInverseProblem_example'"
              << std::endl;
  }

*/

  return;
}
#endif // __EX_STATISTICAL_INVERSE_PROBLEM_APPL_H__
