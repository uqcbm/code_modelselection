clear all;
close all;
clc;

% settings GP
sigma_GP = 1;
covlen_GP = 1;
meas_err = 0.01;

% number of points 
no_pts = 20;
x_dom = linspace(0, 5, no_pts);
y_dom = x_dom;


[X,Y] = meshgrid(x_dom, y_dom);
X_tmp = reshape(X, prod(size(X)), 1);
Y_tmp = reshape(Y, prod(size(Y)), 1);

pts_set = [X_tmp Y_tmp];

% covariance function & sample function
kK = kernVecOrMat_SE_2D( pts_set, pts_set, sigma_GP, covlen_GP, meas_err );
my_func = chol(kK)'*randn(no_pts^2,1);

my_2d_func = reshape( my_func, size(X,1), size(X,2) );


% plot the sample from the Gaussian process
figure; hold on;

surfc( X, Y, my_2d_func );
drawnow;
